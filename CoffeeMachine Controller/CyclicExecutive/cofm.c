#include <stdio.h>
#include "fsm.h"
#include <unistd.h>
#include <time.h>
#include <fcntl.h>

#define PRECIO 50					//Precio del cafe
#define FLAG_BOTON 0x01					//Flag que indica en la variable flags si se ha pulsado el boton de inicio
#define FLAG_ENABLE 0x02				//Flag que indica en la variable flags si el servicio esta habilitado
#define FLAG_DEVOLVER 0x04				//Flag que indica en la variable flags si se debe devolver el cambio
#define TM 8000000					//Periodo del ejecutivo ciclico en ns

enum cofm_state {					//Estados de la cafetera
	COFM_WAITING,					//A la espera de inicio
	COFM_CUP,					//Sirviendo vaso
	COFM_COFFEE,					//Sirviendo cafe
	COFM_MILK,					//Sirviendo leche
};

enum cash_state {					//Estados del monedero: 
	CASH_WAITING,					//A la espera de credito
	CASH_ADQUIRED,					//Introducido credito suficiente
};

static int flags = 0;					//Variable que representa los flags de estado de la maquina de cafe
static int dinero = 0;					//Variable que almacena el credito disponible

static int button_pressed (fsm_t* this){			//Funcion de entrada que inicia el servicio de un cafe
	return ((flags&FLAG_ENABLE)*(flags&FLAG_BOTON));	// si se pulsa el boton estando habilitado
}

static int seguir (fsm_t* this){			//Funcion de entrada usada en lugar de timer
	return 1;					//para simular un comportamiento secuencial
}

static int price_reached (fsm_t* this){			//Funcion de entrada que habilita la secuencia para servir un cafe
	if(dinero >= PRECIO){				//al haberse alcanzado el credito necesario
		flags |= FLAG_ENABLE;
		return 1;
	} else return 0;				//En caso de no alcanzarse, inhabilita el servicio
}

static int devolver_pressed (fsm_t* this){		//Funcion de entrada que indica si debe devolver el cambio
	return (flags & FLAG_DEVOLVER);
}

static void aviso (fsm_t* this){			//Funcion de salida que informa cuando se alcanza el credito necesario y se habilita la secuencia
	printf("\nYa dispone de credito suficiente\n\n");
}

static void devolucion (fsm_t* this){			//Funcion de salida que realiza la devolucion del cambio
	flags = 0;					//Al forzarse la devolucion del cambio, se anula el credito disponible y se inhabilita el boton 
	printf("Devolviendo %d centimos\n\n", dinero);	//que inicia la secuencia para evitar el servicio gratuito. Además se borra el flag de devolución.
	dinero = 0;
}

static void cup (fsm_t* this){				//Funcion de salida del primer estado de la secuencia para servir un cafe
	dinero -= PRECIO;				//Se descuenta el precio del cafe del credito disponible,
	flags &= ~(FLAG_BOTON);				//Se inhabilita la secuencia para servir otro cafe y se quita el flag de boton pulsado
	flags &= ~(FLAG_ENABLE);
	printf("Se apaga el LED.\n");			//Se apaga el LED: indica que la maquina no esta lista para servir otro cafe
	printf("Colocando vaso.\n\n");			//y se coloca el vaso
}

static void coffee (fsm_t* this){			//Funcion de salida del segundo estado de la secuencia para servir un cafe
	printf("Sirviendo cafe.\n\n");			//Se sirve el cafe
}

static void milk (fsm_t* this){				//Funcion de salida del tercer estado de la secuencia para servir un cafe
	printf("Sirviendo leche.\n\n");			//Se sirve la leche
}

static void finish (fsm_t* this){			//Funcion de salida del ultimo estado de la secuencia para servir un cafe
	printf("Cafe servido.\n");			//El cafe esta listo
	printf("Se enciende el LED.\n\n");		//Se enciende el LED: indica que la maquina esta lista para servir otro cafe
	flags |= FLAG_DEVOLVER;				//Se devuelve el credito restante, si lo hubiera
}

// Explicit FSM description
static fsm_trans_t cofm[] = {					//Diagrama de transiciones de la cafetera:
	{ COFM_WAITING, button_pressed, COFM_CUP, cup },	//Salta de WAITING a CUP cuando se detecta inicio habilitado y realiza la funcion cup como salida
	{ COFM_CUP, seguir, COFM_COFFEE, coffee },		//Salta de CUP a COFFEE siempre (a falta de un timer) y realiza la funcion coffee como salida
	{ COFM_COFFEE, seguir, COFM_MILK, milk },		//Salta de COFFEE a MILK siempre y realiza la funcion milk como salida
	{ COFM_MILK, seguir, COFM_WAITING, finish },		//Salta de MILK a WAITING siempre y realiza la funcion finish como salida
	{ -1, NULL, -1, NULL },					//No se contemplan otras transiciones
};

static fsm_trans_t cash[] = {						//Diagrama de transiciones del monedero:
	{ CASH_WAITING, price_reached, CASH_ADQUIRED, aviso },		//Salta de WAITING a ADQUIRED cuando se detecta que hay credito suficiente y realiza la funcion aviso como salida
	{ CASH_WAITING, devolver_pressed, CASH_WAITING, devolucion },	//Se mantiene en WAITING cuando se activa la señal de devolver y realiza la funcion devolucion como salida
	{ CASH_ADQUIRED, devolver_pressed, CASH_WAITING, devolucion },	//Salta de ADQUIRED a WAITING cuando se activa la señal de devolver y realiza la funcion devolucion como salida
	{ -1, NULL, -1, NULL},						//No se contemplan otras transiciones
};

int main () {									//Funcion principal
	fsm_t* cofm_fsm = fsm_new(cofm);
	fsm_t* cash_fsm = fsm_new(cash);

	int input;			//Variable para controlar la entrada de comandos por terminal (inicio, continuacion, devolucion, introduccion de credito)
	long difNanos;			//Variable para facilitar el cálculo del tiempo transcurrido entre dos eventos

	fcntl(0,F_SETFL, fcntl(0,F_GETFL)|O_NONBLOCK);	//Hacemos el scanf no bloqueante

	struct timespec t_before;	//Variable para almacenar el tiempo de inicio de un evento
	struct timespec t_after;	//Variable para almacenar el tiempo de finalizacion de un evento
	struct timespec relleno;	//Variable para almacenar el tiempo que se debe esperar hasta el comienzo del siguiente periodo
	struct timespec resto;		//Variable que recibe el valor restante de tiempo cuando se interrumpe un nanosleep()
	t_before.tv_sec=0;
        t_before.tv_nsec=0;
        t_after.tv_sec=0;
        t_after.tv_nsec=0;


/*	FILE *file_inputs;			//Fichero utilizado como flujo de entrada de comandos
	file_inputs = fopen("./inputs.txt","r");
	if(!file_inputs){
		printf("Error de apertura del archivo inputs.txt\n");
		return 1;
	}
	FILE *ptr_file;				 //Fichero utilizado para almacenar multiples medidas de tiempo
        ptr_file = fopen("./global.txt","a");   
        if(!ptr_file){				 
                printf("Error de apertura del archivo global.txt\n");
                return 1;
        }
*/
	printf("Introduzca el importe necesario y pulse el botón para iniciar el servicio\n");

	while(1){
		clock_gettime(CLOCK_MONOTONIC, &t_before);

		if(scanf("%d",&input)==1){
//		if(fscanf(file_inputs, "%d", &input)==1){

			flags &= ~(FLAG_DEVOLVER);		//Por defecto la señal de devolucion esta inactiva
			flags &= ~(FLAG_BOTON);			//Por defecto el flag del boton pulsado esta inactivo
			if(input>1){				//Para enteros mayores que 1, se considera introduccion de credito y se incrementa el disponible
				dinero += input;
			}else if(input==0){			//El comando '0' representa el boton de devolucion forzada
				flags |= FLAG_DEVOLVER;
			}else if(input==1){			//El comando '1' representa el boton de inicio del servicio
				flags |= FLAG_BOTON;
			}else break; 				//Enteros negativos finalizan el programa
		}
//		clock_gettime(CLOCK_MONOTONIC, &t_after);

//		clock_gettime(CLOCK_MONOTONIC, &t_before);
		fsm_fire(cofm_fsm);
//		clock_gettime(CLOCK_MONOTONIC, &t_after);

//		clock_gettime(CLOCK_MONOTONIC, &t_before);
		fsm_fire(cash_fsm);		
//		clock_gettime(CLOCK_MONOTONIC, &t_after);

		clock_gettime(CLOCK_MONOTONIC, &t_after);

		difNanos = (t_after.tv_nsec - t_before.tv_nsec);
		if(difNanos >= 0){
//			fprintf(ptr_file,"%d\n",(int)(t_after.tv_sec - t_before.tv_sec)*1000000000+(int)(difNanos));
//			printf("\nSe han empleado %d ns del periodo\n",(int)(t_after.tv_sec - t_before.tv_sec)*1000000000+(int)(difNanos));
			relleno.tv_sec = 0;
			relleno.tv_nsec = TM-((int)(t_after.tv_sec-t_before.tv_sec)*1000000000+(int)(difNanos));
		} else {
//			fprintf(ptr_file,"%d\n",(int)(t_after.tv_sec - t_before.tv_sec)*1000000000+(int)(difNanos));
//			printf("\nSe han empleado %d ns del periodo\n",(int)(t_after.tv_sec - t_before.tv_sec)*1000000000+(int)(difNanos));
			relleno.tv_sec = 0;
			relleno.tv_nsec = TM-((int)(t_after.tv_sec-t_before.tv_sec)*1000000000+(int)(-difNanos));
		}

		if(nanosleep(&relleno,&resto)<0){
			printf("Error de relleno\n");
		}//else printf("Se han rellenado %d ns\n\n", (int)(relleno.tv_nsec)); 
	}
//	fclose(file_inputs);
//	fclose(ptr_file);
	return 0;
}
