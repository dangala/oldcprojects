#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <fcntl.h>
#include "fsm.h"
#include "task.h"

/******************* Constants *******************/

#define PRIZE 50					//Precio del cafe
#define FLAG_BUTTON 0x01				//Flag que indica en la variable flags si se ha pulsado el boton de inicio
#define FLAG_ENABLE 0x02				//Flag que indica en la variable flags si el servicio esta habilitado
#define FLAG_RETURN 0x04				//Flag que indica en la variable flags si se debe devolver el cambio
#define T1 20000000					//Periodo de activacion de la tarea 1 (monedero)
#define T2 50000000					//Periodo de activacion de la tarea 2 (atencion de usuario)
#define T3 100000000					//Periodo de activacion de la tarea 3 (maquina de cafe)

/******************* FSMs states *******************/

enum cofm_state {					//Estados de la cafetera
	COFM_WAITING,					//A la espera de inicio
	COFM_CUP,					//Sirviendo vaso
	COFM_COFFEE,					//Sirviendo cafe
	COFM_MILK,					//Sirviendo leche
};

enum cash_state {					//Estados del monedero: 
	CASH_WAITING,					//A la espera de credito
	CASH_ADQUIRED,					//Introducido credito suficiente
};

/******************* Global variables *******************/

static int flags = 0;					//Variable que representa los flags de estado de la maquina de cafe
static int money = 0;					//Variable que almacena el credito disponible
pthread_mutex_t lock_flags;				//Mutex que controla el acceso a la variable compartida flags
pthread_mutex_t lock_money;				//Mutex que controla el acceso a la variable compartida money


/******************* FSMs input functions *******************/

static int button_pressed (fsm_t* this){			//Funcion de entrada que inicia el servicio de un cafe
	pthread_mutex_lock(&lock_flags);
	int pressed = ((flags&FLAG_ENABLE)*(flags&FLAG_BUTTON));	// si se pulsa el boton estando habilitado
	pthread_mutex_unlock(&lock_flags);
	return pressed;
}

static int next (fsm_t* this){			//Funcion de entrada usada en lugar de timer
	return 1;				            //para simular un comportamiento secuencial
}

static int price_reached (fsm_t* this){			//Funcion de entrada que habilita la secuencia para servir un cafe
	int reached = 0;
	pthread_mutex_lock(&lock_money);
	pthread_mutex_lock(&lock_flags);
	if(money >= PRIZE){				//al haberse alcanzado el credito necesario
		flags |= FLAG_ENABLE;
		reached = 1;
	} 
	pthread_mutex_unlock(&lock_money);
	pthread_mutex_unlock(&lock_flags);
	return reached;					//En caso de no alcanzarse, inhabilita el servicio
}

static int return_pressed (fsm_t* this){		//Funcion de entrada que indica si debe devolver el cambio
	pthread_mutex_lock(&lock_flags);
	int pressed = (flags & FLAG_RETURN);
	pthread_mutex_unlock(&lock_flags);
	return pressed;
}

/******************* FSMs output functions *******************/

static void inform (fsm_t* this){			//Funcion de salida que informa cuando se alcanza el credito necesario y se habilita la secuencia
	printf("\nYa dispone de credito suficiente\n\n");
}

static void change (fsm_t* this){			//Funcion de salida que realiza la devolucion del cambio
	pthread_mutex_lock(&lock_flags);
	pthread_mutex_lock(&lock_money);
	flags = 0;					//Al forzarse la devolucion del cambio, se anula el credito disponible y se inhabilita el boton 
	printf("Devolviendo %d centimos\n\n", money);	//que inicia la secuencia para evitar el servicio gratuito. Además se borra el flag de devolución.
	money = 0;
	pthread_mutex_unlock(&lock_flags);
	pthread_mutex_unlock(&lock_money);
}

static void cup (fsm_t* this){				//Funcion de salida del primer estado de la secuencia para servir un cafe
	pthread_mutex_lock(&lock_flags);
	pthread_mutex_lock(&lock_money);
	money -= PRIZE;					//Se descuenta el precio del cafe del credito disponible,
	flags &= ~(FLAG_BUTTON);			//Se inhabilita la secuencia para servir otro cafe y se quita el flag de boton pulsado
	flags &= ~(FLAG_ENABLE);
	pthread_mutex_unlock(&lock_flags);
	pthread_mutex_unlock(&lock_money);
	printf("Se apaga el LED.\n");			//Se apaga el LED: indica que la maquina no esta lista para servir otro cafe
	printf("Colocando vaso.\n\n");			//y se coloca el vaso
}

static void coffee (fsm_t* this){			//Funcion de salida del segundo estado de la secuencia para servir un cafe
	printf("Sirviendo cafe.\n\n");			//Se sirve el cafe
}

static void milk (fsm_t* this){				//Funcion de salida del tercer estado de la secuencia para servir un cafe
	printf("Sirviendo leche.\n\n");			//Se sirve la leche
}

static void finish (fsm_t* this){			//Funcion de salida del ultimo estado de la secuencia para servir un cafe
	pthread_mutex_lock(&lock_flags);
	printf("Cafe servido.\n");			//El cafe esta listo
	printf("Se enciende el LED.\n\n");		//Se enciende el LED: indica que la maquina esta lista para servir otro cafe
	flags |= FLAG_RETURN;				//Se devuelve el credito restante, si lo hubiera
	pthread_mutex_unlock(&lock_flags);
}

/******************* Explicit FSMs descriptions *******************/ 

static fsm_trans_t cofm[] = {                                 //Diagrama de transiciones de la cafetera:
        { COFM_WAITING, button_pressed, COFM_CUP, cup },      //Salta de WAITING a CUP cuando se detecta inicio habilitado y realiza la funcion cup como salida
        { COFM_CUP, next, COFM_COFFEE, coffee },              //Salta de CUP a COFFEE siempre (a falta de un timer) y realiza la funcion coffee como salida
        { COFM_COFFEE, next, COFM_MILK, milk },               //Salta de COFFEE a MILK siempre y realiza la funcion milk como salida
        { COFM_MILK, next, COFM_WAITING, finish },            //Salta de MILK a WAITING siempre y realiza la funcion finish como salida
        { -1, NULL, -1, NULL },                               //No se contemplan otras transiciones
};

static fsm_trans_t cash[] = {                                           //Diagrama de transiciones del monedero:
        { CASH_WAITING, price_reached, CASH_ADQUIRED, inform },          //Salta de WAITING a ADQUIRED cuando se detecta que hay credito suficiente y realiza la funcion inform como salida
        { CASH_WAITING, return_pressed, CASH_WAITING, change },   //Se mantiene en WAITING cuando se activa la señal de devolver y realiza la funcion change como salida
        { CASH_ADQUIRED, return_pressed, CASH_WAITING, change },  //Salta de ADQUIRED a WAITING cuando se activa la señal de devolver y realiza la funcion change como salida
        { -1, NULL, -1, NULL},                                          //No se contemplan otras transiciones
};

/******************* Thread start routines *******************/ 

static void * userEntries(){			//Funcion de inicio del thread que controla los comandos del usuario
	fcntl(0,F_SETFL, fcntl(0,F_GETFL)|O_NONBLOCK);  //Hacemos el scanf no bloqueante

/*      FILE *file_timing;                      //Fichero utilizado para almacenar medidas de tiempos 
        file_timing = fopen("./computeTime2.txt","a");
        if(!file_timing){
                printf("Error abriendo computeTime2.txt\n");
		pthread_exit(NULL);
        }
*/
	int input;
	long difNanos;
	struct timespec t_before, t_after, t_fill, t_remain;
	t_fill.tv_sec = 0;
	while(1){
		clock_gettime(CLOCK_MONOTONIC, &t_before);

		if(scanf("%d",&input)==1){
       		 	pthread_mutex_lock(&lock_flags);
       	        	flags &= ~(FLAG_RETURN);              //Por defecto la señal de devolucion esta inactiva
       	        	flags &= ~(FLAG_BUTTON);                 //Por defecto el flag del boton pulsado esta inactivo
       	        	if(input>1){                            //Para enteros mayores que 1, se considera introduccion de credito y se incrementa el disponible
       	        		pthread_mutex_lock(&lock_money);
       	                	money += input;
       	                	pthread_mutex_unlock(&lock_money);
       	        	}else if(input==0){                     //El comando '0' representa el boton de devolucion forzada
       	                	flags |= FLAG_RETURN;
       	        	}else if(input==1){                     //El comando '1' representa el boton de inicio del servicio
       	                	flags |= FLAG_BUTTON;
       	        	}else{
				pthread_mutex_unlock(&lock_flags);
				pthread_exit(NULL);
			}
       	        	pthread_mutex_unlock(&lock_flags);
		}
		clock_gettime(CLOCK_MONOTONIC, &t_after);
		difNanos = t_after.tv_nsec - t_before.tv_nsec;
		if(difNanos >= 0){
			t_fill.tv_nsec = T1 - ((long)((t_after.tv_sec - t_before.tv_sec)*1000000000) + difNanos);
//			fprintf(file_timing, "%ld ns\n", t_fill.tv_nsec);
		} else {
			t_fill.tv_nsec = T1 - ((long)((t_after.tv_sec - t_before.tv_sec - 1)*1000000000) - difNanos);
//			fprintf(file_timing, "%ld ns\n", t_fill.tv_nsec);
		}
		if(nanosleep(&t_fill,&t_remain)<0) printf("Error de relleno tarea2\n");
	}
//	fclose(file_timing);
	pthread_exit(NULL);
}

static void * fireCofm(){			//Funcion de inicio del thread que controla la maquina de cafe
	fsm_t* cofm_fsm = fsm_new(cofm);

/*	FILE *file_timing;
	file_timing = fopen("./computeTime3.txt","a");
	if(!file_timing){
		printf("Error abriendo computeTime3.txt\n");
		pthread_exit(NULL);
	}
*/
	struct timespec t_before, t_after, t_fill, t_remain;
	long difNanos;
	t_fill.tv_sec = 0;
        while(1){
		clock_gettime(CLOCK_MONOTONIC, &t_before);
		fsm_fire(cofm_fsm);
		clock_gettime(CLOCK_MONOTONIC, &t_after);
		difNanos = t_after.tv_nsec - t_before.tv_nsec;
		if(difNanos >= 0){
			t_fill.tv_nsec = T2 - ((long)((t_after.tv_sec - t_before.tv_sec)*1000000000)+difNanos);
//			fprintf(file_timing, "%ld ns\n", t_fill.tv_nsec);
		} else {
			t_fill.tv_nsec = T2 - ((long)((t_after.tv_sec - t_before.tv_sec - 1)*1000000000)-difNanos);	
//			fprintf(file_timing, "%ld ns\n", t_fill.tv_nsec);
		}
		if(nanosleep(&t_fill,&t_remain)<0) printf("Error de relleno tarea2\n");
	}
//	fclose(file_timing);
	pthread_exit(NULL);
}

static void * fireCash(){			//Funcion de inicio del thread que controla el monedero
	fsm_t* cash_fsm = fsm_new(cash);

/*	FILE *file_timing;
	file_timing = fopen("./computeTime1.txt","a");
	if(!file_timing){
		printf("Error abriendo computeTime1.txt\n");
		pthread_exit(NULL);
	}
*/
	struct timespec t_before, t_after, t_fill, t_remain;
	long difNanos;
	t_fill.tv_sec = 0;
        while(1){
		clock_gettime(CLOCK_MONOTONIC, &t_before);
		fsm_fire(cash_fsm);
		clock_gettime(CLOCK_MONOTONIC, &t_after);
		difNanos = t_after.tv_nsec - t_before.tv_nsec;
		if(difNanos>=0){
			t_fill.tv_nsec = T3 - ((long)((t_after.tv_sec - t_before.tv_sec)*1000000000)+difNanos);
//			fprintf(file_timing, "%ld ns\n", t_fill.tv_nsec);
		} else {
			t_fill.tv_nsec = T3 - ((long)((t_after.tv_sec - t_before.tv_sec - 1)*1000000000)-difNanos);
//			fprintf(file_timing, "%ld ns\n", t_fill.tv_nsec);
		}
		if(nanosleep(&t_fill,&t_remain)<0) printf("Error de relleno tarea3\n");
	}
//	fclose(file_timing);
	pthread_exit(NULL);
}

/******************* Main programm *******************/ 

int main () {

	mutex_init(&lock_flags, 0);		//Inicializamos los mutex para funcionar
	mutex_init(&lock_money, 0);		//en modo herencia de prioridad

	printf("Introduzca el importe necesario y pulse el botón para iniciar el servicio\n");

	task_new("task1", fireCash, 3);				//Creamos 3 threads: uno para controlar el monedero,
	pthread_t tid2 = task_new("task2", userEntries, 2);	//otro para atender al usuario
	task_new("task3", fireCofm, 1);				//y otro para controlar la maquina de cafe

	pthread_join(tid2, NULL);		//Hacemos que el programa principal espere a que los threads acaben 

	return 0;
}
