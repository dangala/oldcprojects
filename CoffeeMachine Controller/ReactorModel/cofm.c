#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <fcntl.h>
#include "fsm.h"
#include "reactor.h"

/******************* Constants *******************/

#define PRIZE 50					//Precio del cafe
#define FLAG_BUTTON 0x01				//Flag que indica en la variable flags si se ha pulsado el boton de inicio
#define FLAG_ENABLE 0x02				//Flag que indica en la variable flags si el servicio esta habilitado
#define FLAG_RETURN 0x04				//Flag que indica en la variable flags si se debe devolver el cambio
#define T1 500						//Periodo de activacion de la tarea 1 (monedero) en us
#define T2 500						//Periodo de activacion de la tarea 2 (usuario) en us
#define T3 500						//Periodo de activacion de la tarea 3 (cafetera) en us

/******************* FSMs states *******************/

enum cofm_state {					//Estados de la cafetera
	COFM_WAITING,					//A la espera de inicio
	COFM_CUP,					//Sirviendo vaso
	COFM_COFFEE,					//Sirviendo cafe
	COFM_MILK,					//Sirviendo leche
};

enum cash_state {					//Estados del monedero: 
	CASH_WAITING,					//A la espera de credito
	CASH_ADQUIRED,					//Introducido credito suficiente
};

/******************* Shared Resources *******************/

static int flags = 0;					//Variable que representa los flags de estado de la maquina de cafe
static int money = 0;					//Variable que almacena el credito disponible


/******************* FSMs input functions *******************/

static int button_pressed (fsm_t* this){			//Funcion de entrada que inicia el servicio de un cafe
	return ((flags&FLAG_ENABLE)*(flags&FLAG_BUTTON));	// si se pulsa el boton estando habilitado
}

static int next (fsm_t* this){			//Funcion de entrada usada en lugar de timer
	return 1;				            //para simular un comportamiento secuencial
}

static int price_reached (fsm_t* this){			//Funcion de entrada que habilita la secuencia para servir un cafe
	if(money >= PRIZE){				//al haberse alcanzado el credito necesario
		flags |= FLAG_ENABLE;
		return 1;
	} 
	return 0;					//En caso de no alcanzarse, inhabilita el servicio
}

static int return_pressed (fsm_t* this){		//Funcion de entrada que indica si debe devolver el cambio
	return (flags & FLAG_RETURN);
}

/******************* FSMs output functions *******************/

static void inform (fsm_t* this){			//Funcion de salida que informa cuando se alcanza el credito necesario y se habilita la secuencia
	printf("\nYa dispone de credito suficiente\n\n");
}

static void change (fsm_t* this){			//Funcion de salida que realiza la devolucion del cambio
	flags = 0;					//Al forzarse la devolucion del cambio, se anula el credito disponible y se inhabilita el boton 
	printf("Devolviendo %d centimos\n\n", money);	//que inicia la secuencia para evitar el servicio gratuito. Además se borra el flag de devolución.
	money = 0;
}

static void cup (fsm_t* this){				//Funcion de salida del primer estado de la secuencia para servir un cafe
	money -= PRIZE;					//Se descuenta el precio del cafe del credito disponible,
	flags &= ~(FLAG_BUTTON);			//Se inhabilita la secuencia para servir otro cafe y se quita el flag de boton pulsado
	flags &= ~(FLAG_ENABLE);
	printf("Se apaga el LED.\n");			//Se apaga el LED: indica que la maquina no esta lista para servir otro cafe
	printf("Colocando vaso.\n\n");			//y se coloca el vaso
}

static void coffee (fsm_t* this){			//Funcion de salida del segundo estado de la secuencia para servir un cafe
	printf("Sirviendo cafe.\n\n");			//Se sirve el cafe
}

static void milk (fsm_t* this){				//Funcion de salida del tercer estado de la secuencia para servir un cafe
	printf("Sirviendo leche.\n\n");			//Se sirve la leche
}

static void finish (fsm_t* this){			//Funcion de salida del ultimo estado de la secuencia para servir un cafe
	printf("Cafe servido.\n");			//El cafe esta listo
	printf("Se enciende el LED.\n\n");		//Se enciende el LED: indica que la maquina esta lista para servir otro cafe
	flags |= FLAG_RETURN;				//Se devuelve el credito restante, si lo hubiera
}

/******************* Explicit FSMs descriptions *******************/ 

static fsm_trans_t cofm[] = {                                 //Diagrama de transiciones de la cafetera:
        { COFM_WAITING, button_pressed, COFM_CUP, cup },      //Salta de WAITING a CUP cuando se detecta inicio habilitado y realiza la funcion cup como salida
        { COFM_CUP, next, COFM_COFFEE, coffee },              //Salta de CUP a COFFEE siempre (a falta de un timer) y realiza la funcion coffee como salida
        { COFM_COFFEE, next, COFM_MILK, milk },               //Salta de COFFEE a MILK siempre y realiza la funcion milk como salida
        { COFM_MILK, next, COFM_WAITING, finish },            //Salta de MILK a WAITING siempre y realiza la funcion finish como salida
        { -1, NULL, -1, NULL },                               //No se contemplan otras transiciones
};

static fsm_trans_t cash[] = {                                           //Diagrama de transiciones del monedero:
        { CASH_WAITING, price_reached, CASH_ADQUIRED, inform },          //Salta de WAITING a ADQUIRED cuando se detecta que hay credito suficiente y realiza la funcion inform como salida
        { CASH_WAITING, return_pressed, CASH_WAITING, change },   //Se mantiene en WAITING cuando se activa la señal de devolver y realiza la funcion change como salida
        { CASH_ADQUIRED, return_pressed, CASH_WAITING, change },  //Salta de ADQUIRED a WAITING cuando se activa la señal de devolver y realiza la funcion change como salida
        { -1, NULL, -1, NULL},                                          //No se contemplan otras transiciones
};

/******************* Event Handler routines *******************/ 

static void fireCofm(struct event_handler_t* this){			//Event Handler que controla la maquina de cafe

/*	struct timespec t_before, t_after;

	FILE *file_timing; 		                     //Fichero utilizado para almacenar medidas de tiempos
        file_timing = fopen("./computeTime3.txt","a");
        if(!file_timing){
                printf("Error abriendo computeTime3.txt\n");
        }

	long difNanos;

	clock_gettime(CLOCK_MONOTONIC, &t_before);
*/	static const struct timeval period = {0, T3};
	fsm_fire((fsm_t*)(this->user_data));
	timeval_add(&this->next_activation, &this->next_activation, &period);	//Actualizamos la siguiente activacion de la tarea
/*	clock_gettime(CLOCK_MONOTONIC, &t_after);
	difNanos = t_after.tv_nsec - t_before.tv_nsec;
	if(difNanos >= 0){
		fprintf(file_timing, "%ld ns\n", (long)(t_after.tv_sec-t_before.tv_sec)*1000000000+difNanos);
	} else {
		fprintf(file_timing, "%ld ns\n", (long)(t_after.tv_sec-t_before.tv_sec-1)*1000000000-difNanos);
	}
	fclose(file_timing); */
}

static void fireCash(struct event_handler_t* this){			//Event Handler que controla el monedero

/*	struct timespec t_before, t_after;

	FILE *file_timing;     		                 //Fichero utilizado para almacenar medidas de tiempos
        file_timing = fopen("./computeTime1.txt","a");
        if(!file_timing){
                printf("Error abriendo computeTime1.txt\n");
        }
	long difNanos;

	clock_gettime(CLOCK_MONOTONIC,&t_before);
*/	static const struct timeval period = {0, T1};
	fsm_fire((fsm_t*)(this->user_data));
	timeval_add(&this->next_activation, &this->next_activation, &period);	//Actualizamos la siguiente activacion de la tarea
/*	clock_gettime(CLOCK_MONOTONIC, &t_after);
	difNanos = t_after.tv_nsec - t_before.tv_nsec;
	if(difNanos>=0){
		fprintf(file_timing, "%ld ns\n", (long)(t_after.tv_sec-t_before.tv_sec)*1000000000+difNanos);
	} else {
		fprintf(file_timing, "%ld ns\n", (long)(t_after.tv_sec-t_before.tv_sec-1)*1000000000-difNanos);
	}
	fclose(file_timing); */
}

static void userEntries(struct event_handler_t* this){		//Event Handler que controla las entradas de usuario

/*	struct timespec t_before, t_after;

	FILE *file_timing;				//Fichero utilizado para almacenar medidas de tiempos
	file_timing = fopen("./computeTime2.txt","a");
	if(!file_timing){
		printf("Error abriendo computeTime2.txt\n");
	}
	long difNanos;
	clock_gettime(CLOCK_MONOTONIC, &t_before);
*/
	fcntl(0, F_SETFL, fcntl(0,F_GETFL)|O_NONBLOCK);	//Hacemos el scanf no bloqueante
	static const struct timeval period = {0, T2};
	int input;
	if(scanf("%d",&input)==1){
	        flags &= ~(FLAG_RETURN);                //Por defecto la señal de devolucion esta inactiva
                flags &= ~(FLAG_BUTTON);                //Por defecto el flag del boton pulsado esta inactivo
                if(input>1){                            //Para enteros mayores que 1, se considera introduccion de credito y se incrementa el disponible
                	money += input;
                }else if(input==0){                     //El comando '0' representa el boton de devolucion forzada
                        flags |= FLAG_RETURN;
                }else if(input==1){                     //El comando '1' representa el boton de inicio del servicio
                        flags |= FLAG_BUTTON;
                }else{
                        exit(0);			//Otros valores de entrada finalizan el programa
                }
        }
	timeval_add(&this->next_activation, &this->next_activation, &period);	//Actualizamos la siguiente activacion de la tarea
/*	clock_gettime(CLOCK_MONOTONIC, &t_after);
	difNanos = t_after.tv_nsec - t_before.tv_nsec;
	if(difNanos>=0){
		fprintf(file_timing, "%ld ns\n", (long)(t_after.tv_sec-t_before.tv_sec)*1000000000+difNanos);
	} else {
		fprintf(file_timing, "%ld ns\n", (long)(t_after.tv_sec-t_before.tv_sec-1)*1000000000-difNanos);
	}
	fclose(file_timing); */
}

/******************* Main programm *******************/ 

int main () {

	fsm_t* cash_fsm = fsm_new(cash);
	fsm_t* cofm_fsm = fsm_new(cofm);

	EventHandler eh1, eh2, eh3;
	reactor_init();

	event_handler_init(&eh1, 3, fireCash, (void*)(cash_fsm));
	event_handler_init(&eh2, 4, userEntries, NULL);
	event_handler_init(&eh3, 1, fireCofm, (void*)(cofm_fsm));

	reactor_add_handler(&eh1);
	reactor_add_handler(&eh2);
	reactor_add_handler(&eh3);

	printf("Introduzca el importe necesario y pulse el botón para iniciar el servicio\n");

	while(1){
		reactor_handle_events();
	}
	
	return 0;
}
